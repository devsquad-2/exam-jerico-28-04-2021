package user.dao;

import user.pojo.User;

public class UserLoginDAOAuthenticationImplementation implements UserLoginDAOInterface {
	
	private boolean authenticationStatus = false;

	@Override
	public boolean userLoginAuthentication(User refUser) {
		
		// allow only the correct userID and userPassword, else not authenticated
		if (refUser.getUserID().equals("jerico.navarro@theoptimum.net") && refUser.getUserPassword().equals("abc123!!")) {
			authenticationStatus = true;
			
		} else {
			authenticationStatus = false;
		}
		return authenticationStatus;
	}

}
