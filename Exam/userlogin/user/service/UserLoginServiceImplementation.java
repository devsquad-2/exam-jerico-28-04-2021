package user.service;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

import user.dao.UserLoginDAOAuthenticationImplementation;
import user.dao.UserLoginDAOInterface;
import user.pojo.User;

public class UserLoginServiceImplementation implements UserLoginServiceInterface {
	
	UserLoginDAOInterface refUserLoginDAOInterface = null;
	User refUser;
	boolean userChoice = false;
	double amountPayable = 1000.00;
	
	@Override
	public void callUserDAOImplementation() {
		refUser = new User();
		var userID = "";
		var userPassword = "";
		Scanner sc = new Scanner(System.in);
		
		try {
			System.out.println("Enter userID: ");
			userID = sc.next();
			userChoice = true;
			
		// catch any invalid userID input such as integers and empty strings
		} catch (InputMismatchException | NullPointerException e) {
			System.out.println("Invalid userID.");
			userChoice = false;
		
		}
		
		// do-while loop to ensure user keys in correct userID
//		do {
//
//			try {
//				System.out.println("Enter userID: ");
//				userID = sc.next();
//				userChoice = true;
//				
//			} catch (InputMismatchException | NullPointerException e) {
//				System.out.println("Invalid userID.");
//				userChoice = false;
//			}
//			
//		} while (!userChoice);
		
		// catch any invalid userPassword input such as integers and empty strings
		try {
			System.out.println("Enter password: ");
			userPassword = sc.next();
			userChoice = true;
			
		} catch (InputMismatchException | NullPointerException e) {
			System.out.println("Invalid password.");
			userChoice = false;
		
		}
		
		// do-while loop to ensure user keys in correct userPassword
//		do {
//
//			try {
//				System.out.println("Enter password: ");
//				userPassword = sc.next();
//				userChoice = true;
//				
//			} catch (InputMismatchException | NullPointerException e) {
//				System.out.println("Invalid password.");
//				userChoice = false;
//			}
//			
//		} while (!userChoice);
		
		refUser.setUserID(userID);
		refUser.setUserPassword(userPassword);
		
		refUserLoginDAOInterface = new UserLoginDAOAuthenticationImplementation();
		refUserLoginDAOInterface.userLoginAuthentication(refUser); 
				
		if (refUserLoginDAOInterface.userLoginAuthentication(refUser)) {
			System.out.println("Successfully Login");
			System.out.println("Payment Due Date: 30-Apr-2021");
			
			System.out.println("Enter today's date: (DD-MM-YYYY)");
			String date = sc.next();
			Date dateToday = new Date();
			Date dueDate = new Date();
			
			try {
				dateToday = new SimpleDateFormat("dd-MM-yyyy").parse(date);
				dueDate = new SimpleDateFormat("dd-MM-yyyy").parse("30-04-2021");
			} catch (ParseException e) {
				System.out.println(e);
			}
			
			if (dateToday.after(dueDate)) {
				amountPayable *= 1.05;
				System.out.println("Total Payable Amount: " + amountPayable);
			} else {
				System.out.println("Total Payable Amount: " + amountPayable);
			}
		
		} else {
			System.out.println("Sorry invalid Credentials");
		}
		sc.close();
	}
}
