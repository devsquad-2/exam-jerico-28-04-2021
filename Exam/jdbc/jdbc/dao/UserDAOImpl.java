package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jdbc.pojo.User;
import utility.DBUtility;

public class UserDAOImpl implements UserDAO{

	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	
	@Override
	public void getUserRecord() {
		
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "select customerID,customerPassword from customer";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			ResultSet results = refPreparedStatement.executeQuery();  

			System.out.println("Your Customer list is as follows: ");
			System.out.println(" ");
			System.out.printf("%-10s %-10s \n", "Customer ID", "Customer Password");
			
			while (results.next()) {
				String customerID = results.getString("customerID");
				String customerPassword = results.getString("customerPassword");
				System.out.printf("%-10d %-10s \n", customerID, customerPassword);
			}
			
		} catch (SQLException e) {
			System.out.println("Exception handled while getting all customer records.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertRecord(User refUser) {
				
		try {
			refConnection = DBUtility.getConnection();
			
			String sqlQuery = "insert into customer(customerID,customerPassword) values(?,?)";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refUser.getCustomerID());
			refPreparedStatement.setString(2, refUser.getCustomerPassword());
			
			refPreparedStatement.execute();  
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Exception Handled while insert record.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing Connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	} 

	@Override
	public void deleteRecord(int input) {
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "delete from customer where customerID=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1,input);

			refPreparedStatement.execute();  
	
		} catch (SQLException e) {
			System.out.println("Exception Handled while deleting record.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing Connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void updateRecord(String userID, String newPassword) {
		try {
			refConnection = DBUtility.getConnection();	
			
			String sqlQuery = "update customer set customerPassword=? where customerID=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, newPassword);
			refPreparedStatement.setString(2, customerID);

			refPreparedStatement.execute();  

			
		} catch (SQLException e) {
			System.out.println("Exception Handled while deleting record.");
		}
		
		finally {
			try {
				refConnection.close();
				System.out.println("Closing Connection..");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public boolean userLogin(String customerID, String customerPassword) {
		
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "SELECT customerID FROM customer WHERE customerID = ? AND customerPassword = ? LIMIT 1";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, customerID);
			refPreparedStatement.setString(2, customerPassword);

			ResultSet rs = refPreparedStatement.executeQuery();  
			
			if(rs.next()) {
				return true;
			} else {
				return false;
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Exception Handled while getting all user record.");
			return false;
		}
	}

	
	@Override
	public void userLogout() {
		try {
			refConnection = DBUtility.getConnection();	
			refConnection.close();
			System.out.println("Closing Connection.. Have a nice day.");			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
