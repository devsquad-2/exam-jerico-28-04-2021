package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException; 

import POJO.Customer;
import Utility.DBUtility;

public class customerDAOImplementation implements CustomerDAO {
	
	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;
	
	@Override
	public void insertRecord(Customer refCustomer) {
		try {
			refConnection = DBUtility.getConnection();
			
			String sqlQuery = "insert into customer(userID,password) values(?,?)";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refCustomer.getUserID());
			refPreparedStatement.setString(2, refCustomer.getPassword());
			
			refPreparedStatement.execute();  
			
			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Exception Handled while insert record.");
		}
	}
	
	@Override 
	public void userLogOut() {
		System.out.println("Have a nice day");
	}
	@Override
	public boolean userLogin(String userID, String password) {
		
		try {
			refConnection = DBUtility.getConnection();
			
			String sqlQuery = "select * from customer where userID = ? and password = ?";
			
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, userID);
			refPreparedStatement.setString(2, password);
			
			ResultSet rset = refPreparedStatement.executeQuery();
			
			while(rset.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("Exception handled while getting all user record");
			} 
		return false;
	}
}
